# FULL STACK TEST

## Before start!
1. Clone the project:
`git clone git@gitlab.com:f.orregoibarra/prueba-seleccion-fullstack.git`
2. Enter to project: `cd prueba-seleccion-fullstack`

## Backend:
1. Go to `/backend` folder with `cd backend` and rename `.env.example` to `.env`
2. Install `/backend` dependencies with: `npm install`
1. Inside `/backend` folder run: `npm start`

>  Optionally we have two scrips for `/backend`.

1. `npm run remove` to remove MongoDB collection
2. `npm run sync` to fill MongoDB collecion

## Frontend:
1. Go to `/frontend` folder with `cd frontend` and rename `.env.example` to `.env`
2. Install `/frontend` dependencies with: `npm install`
3. Inside `/frontend` folder run: `npm start`
4. Go to `http://localhost:3000`