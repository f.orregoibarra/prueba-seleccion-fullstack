import React from 'react'
import CardDetail from '../../components/CardDetail/CardDetail';
import { Link } from 'react-router-dom';
import { Button } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import styles from './style/Content.js';

const useStyles = makeStyles(styles);

const Content = (props) => {
  const { character } = props;

  const classes = useStyles();

  return (
    <>
      <div className={classes.div}>
        <Link to='/characters/'>
          <Button className={classes.button}>
            {"BACK"}
          </Button>
        </Link>
      </div>
      <CardDetail character={character}/>
    </>
  );
}

export default Content;