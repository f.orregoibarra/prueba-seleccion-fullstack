import React, { useEffect, useState } from 'react';
import StatePageWrapper from '../../components/ViewWrapper/ViewWrapper.jsx';
import Content from './Content.jsx';
import { getOneCharacter } from '../../provider/provider';
import { useParams } from 'react-router-dom';

const CharacterDetail = () => {
	const [state, setState] = useState('LOADING');
	const [character, setCharacter] = useState([]);

	const { id } = useParams();

	useEffect(() => {
		getOneCharacter(id)
			.then(data => {
				setCharacter(data);
				setState('SUCCESS');
			})
			.catch(error => {
				setState('ERROR');
			});
	}, []);

	return (
		<StatePageWrapper state={state}>
			<Content character={character} />
		</StatePageWrapper>
	);
};

export default CharacterDetail;