import React, { useEffect, useState } from 'react'
import { Pagination } from '@material-ui/lab';
import Searcher from '../../components/Searcher/Searcher.jsx';
import CardList from '../../components/CardList/CardList.jsx';
import { Grid } from '@material-ui/core';
import { useMediaQuery } from 'react-responsive'

import { makeStyles } from '@material-ui/core/styles';
import styles from './style/Content.js';

const useStyles = makeStyles(styles);

const Content = (props) => {
  const { characters: charactersProps } = props;

  const [characters, setCharacters] = useState([]);
  const [page, setPage] = useState(1);

  const classes = useStyles();
  const isMobile = useMediaQuery({ query: '(max-width: 959px)' });

  const rowsByPage = 10;

  const handleSearch = inputSearch => {
    const searchFilter = charactersProps.filter(charactersProp => 
      (
        charactersProp.house.toLowerCase().indexOf(inputSearch.toLowerCase()) !== -1
        ||
        charactersProp.name.toLowerCase().indexOf(inputSearch.toLowerCase()) !== -1
      )
    );
    setCharacters(searchFilter);
  }
  
  useEffect(() => {
    setCharacters(charactersProps);
  }, [charactersProps])

  return (
    <Grid container spacing={4}>
      <Grid item xs={12} md={12}>
        <Searcher handleSearch={handleSearch}/>
      </Grid>
      <Grid item xs={12} md={12}>
        <CardList characters={characters} page={page} rowsByPage={rowsByPage}/>
      </Grid>
      <Grid item xs={12} md={12}>
        <Pagination
          count={Math.ceil((characters.length)/rowsByPage)}
          variant="outlined"
          shape="rounded"
          onChange={(event, value) => setPage(value)}
          className={classes.pagination}
          showFirstButton={!isMobile}
          showLastButton={!isMobile}
        />
      </Grid>
    </Grid>
  );
}

export default Content;