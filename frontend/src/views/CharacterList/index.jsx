import React, { useEffect, useState } from 'react';
import StatePageWrapper from '../../components/ViewWrapper/ViewWrapper.jsx';
import Content from './Content.jsx';
import { getAllCharacters } from '../../provider/provider';

const CharacterList = () => {
	const [state, setState] = useState('LOADING');
	const [characters, setCharacters] = useState([]);

	useEffect(() => {
		getAllCharacters()
			.then(data => {
				setCharacters(data);
				setState('SUCCESS');
			})
			.catch(error => {
				setState('ERROR');
			});
	}, []);


	return (
    <StatePageWrapper state={state}>
			<Content characters={characters} />
		</StatePageWrapper>
	);
};

export default CharacterList;