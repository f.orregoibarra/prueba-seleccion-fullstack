import axios from 'axios'

const client = axios.create({
  baseURL: process.env.REACT_APP_API_GOT_URL,
  timeout: 15000,
  headers: {
    'Content-Type': 'application/json',
  },
})

export const response = (options) => {
  return new Promise((resolve, reject) => {
    try {
			client(options)
				.then(data => resolve(data.data))
				.catch(error => reject(error));
		} catch (error) {
			reject(error);
		}
  })
}

export default client
