import { response } from './client.js';

export const getAllCharacters = () => {
	const options = {
		url: '/characters',
	};
	return response(options);
};

export const getOneCharacter = id => {
	const options = {
		url: `/characters/${id}`,
	};
	return response(options);
};