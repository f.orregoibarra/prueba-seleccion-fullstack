import { Redirect, Route, BrowserRouter, Switch } from 'react-router-dom';
import RouterWrapper from './routes/RouterWrapper.jsx';

import CharacterList from './views/CharacterList';
import CharacterDetail from './views/CharacterDetail';
import Error from './components/Error/Error.jsx';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <RouterWrapper exact path='/characters/' component={CharacterList}/>
        <RouterWrapper exact path='/characters/:id' component={CharacterDetail}/>
        <Route exact path="/"><Redirect to="/characters" /></Route>
        <Route component={Error} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;