import React from 'react';
import { Route } from 'react-router-dom';
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles'

import styles from './style/RouterWrapperStyle';

const useStyles = makeStyles(styles);

const RouteWrapper = props => {
	const { component: Component, ...rest } = props;
	const classes = useStyles();

	return (
		<Route {...rest} render={matchProps => {
			return (
				<Container className={classes.container}>
					<Component {...matchProps} />
				</Container>
			)
		}} />
	);
};

export default RouteWrapper;
