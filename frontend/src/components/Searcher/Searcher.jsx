import React, { useState } from 'react'
import {
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';

import { makeStyles } from '@material-ui/core/styles'

import styles from './style/Searcher.js';

const useStyles = makeStyles(styles);

const Searcher = (props) => {
  const { handleSearch } = props;
  const [inputSearch, setInputSearch] = useState('');
  const classes = useStyles();

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      handleSearch(inputSearch);
    }
  }

  const handleClearSearch = () => {
    setInputSearch('');
    handleSearch('');
  }
  
  return (
    <FormControl fullWidth variant="outlined">
      <InputLabel htmlFor="search-by">Search</InputLabel>
      <OutlinedInput
        id="search-by"
        onChange={(event) => setInputSearch(event.target.value)}
        onKeyDown={handleKeyDown}
        labelWidth={60}
        value={inputSearch}
        endAdornment={
          <InputAdornment position="end">
            {inputSearch === "" ? null :
              <IconButton
                aria-label="search icon"
                onClick={() => handleClearSearch()}
                edge="end"
                classes={{edgeEnd: classes.icon}}
              >
                <ClearIcon />
              </IconButton>
              }
            <IconButton
              aria-label="search icon"
              onClick={() => handleSearch(inputSearch)}
              edge="end"
            >
              <SearchIcon />
            </IconButton>
          </InputAdornment>
        }
      />
    </FormControl>
  );
}

export default Searcher;
