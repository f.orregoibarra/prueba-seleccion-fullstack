import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import styles from './style/Error.js';

const useStyles = makeStyles(styles);

const CardList = props => {
  const { characters, page, rowsByPage } = props;
  const classes = useStyles();
  
  return (
    <Grid container direction="column">
      <Typography variant="h2" align="center">
        {"Error 404"}
      </Typography>
      <Typography variant="h3" align="center">
        {"Ops! Not found!"}
      </Typography>
    </Grid>
	);
};

export default CardList;

