import React from 'react';
import { CircularProgress } from '@material-ui/core';
import Error from '../Error/Error.jsx';

import { makeStyles } from '@material-ui/core/styles';
import styles from './style/ViewWrapper.js';

const useStyles = makeStyles(styles);

const ViewWrapper = props => {
	const { state, children } = props;
	const classes = useStyles();

	const renderDetail = () => {
		switch (state) {
			case 'LOADING':
				return <div className={classes.div}><CircularProgress/></div>;
			case 'SUCCESS':
				return children;
			default:
				return <div className={classes.div}><Error/></div>;
		}
	};

	return (
    <>{renderDetail()}</>
  )
};

export default ViewWrapper;
