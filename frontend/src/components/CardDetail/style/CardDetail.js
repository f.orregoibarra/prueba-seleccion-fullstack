const style = {
  avatarRoot: {
    width: 100,
    height: 500,
    overflow: 'hidden'
  },
  avatarRounded: {
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0
  },
  details: {
    display: 'flex',
    flexDirection: 'row',
  },
  paper: {
    margin: 'auto',
    width: 800
  },
  cardContent: {
    flex: '1 0 auto',
  },
  gridContainer: {
    padding: 10
  },
  typographyAlign: {
    display: 'flex',
    textTransform: 'capitalize'
  },
  icon: {
    color: 'gray',
    width: 22,
    height: 22,
    marginRight: 4
  },
  chipBook: {
    color: 'brown',
    borderColor: 'brown'
  },
  chipTitle: {
    color: 'goldenrod',
    borderColor: 'goldenrod'
  },
}

export default style