import React from 'react';
import { Avatar, Chip, Grid, Paper, Typography } from '@material-ui/core';
import { useMediaQuery } from 'react-responsive'

import { makeStyles } from '@material-ui/core/styles'
import HomeIcon from '@material-ui/icons/Home';
import WcIcon from '@material-ui/icons/Wc';
import StarIcon from '@material-ui/icons/Star';

import styles from './style/CardDetail';
const useStyles = makeStyles(styles);

const CardDetail = props => {
  const { name, slug, house, gender, image, pagerank, books, titles } = props.character;
  const classes = useStyles();

  const isMobile = useMediaQuery({ query: '(max-width: 959px)' });

  return (
    <Paper className={isMobile ? '' : classes.paper}>
      <div className={classes.details}>
        <Avatar
          src={image}
          variant="rounded"
          alt={name}
          classes={{root: classes.avatarRoot, rounded: classes.avatarRounded}}>
            {name[0]}
        </Avatar>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={12} md={12}>
            <Typography
              align="center"
              variant={isMobile ? "h6" : "h4"}
            >
              {name}
            </Typography>
            <Typography
              className={classes.gridContainer}
              align="center"
              variant={isMobile ? "body1" : "h6"}
            >
              {slug}
            </Typography>
            
            <Grid container spacing={1}>
              <Grid item xs={12} md={4}>
                <Typography className={classes.typographyAlign}>
                  <HomeIcon className={classes.icon}/> {house !== '' ? house : 'unknown'}
                </Typography>
                <Typography className={classes.typographyAlign}>
                  <WcIcon className={classes.icon}/> {gender ? gender : 'unknown'}
                </Typography>
                <Typography className={classes.typographyAlign}>
                  <StarIcon className={classes.icon}/> {pagerank.rank}
                </Typography>
              </Grid>
              <Grid item xs={12} md={4}>
                <Typography className={classes.typographyAlign}>
                  {"Books"}
                </Typography>
                {books.map((book, index) => (
                  <Chip key={index} className={classes.chipBook} label={book} variant="outlined" />
                ))}
              </Grid>
              <Grid item xs={12} md={4}>
                <Typography>
                  {"Titles"}
                </Typography>
                {titles.map((title, index) => (
                  <Chip key={index} className={classes.chipTitle} label={title} variant="outlined" />
                ))}
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </Paper>
	);
};

export default CardDetail;