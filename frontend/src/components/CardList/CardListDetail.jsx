import React from 'react';
import { Avatar, Grid, Paper, Typography } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import HomeIcon from '@material-ui/icons/Home';
import WcIcon from '@material-ui/icons/Wc';
import StarIcon from '@material-ui/icons/Star';

import styles from './style/CardListDetail';

const useStyles = makeStyles(styles);

const CardDetail = props => {
  const { name, house, gender, image, pagerank } = props.character;
  const classes = useStyles();
  
  return (
    <Paper>
      <div className={classes.details}>
        <Avatar
          src={image}
          variant="rounded"
          alt={name}
          classes={{root: classes.avatarRoot, rounded: classes.avatarRounded}}>
            {name[0]}
        </Avatar>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={12} md={12}>
            <Typography align="center" component="h6" variant="h6">
              {name}
            </Typography>
          </Grid>
          <Grid item xs={12} md={12}>
            <Typography className={classes.typographyAlign}>
              <HomeIcon className={classes.icon}/> {house !== '' ? house : 'unknown'}
            </Typography>
            <Typography className={classes.typographyAlign}>
              <WcIcon className={classes.icon}/> {gender ? gender : 'unknown'}
            </Typography>
            <Typography className={classes.typographyAlign}>
              <StarIcon className={classes.icon}/> {pagerank.rank}
            </Typography>
          </Grid>
        </Grid>
      </div>
    </Paper>
	);
};

export default CardDetail;