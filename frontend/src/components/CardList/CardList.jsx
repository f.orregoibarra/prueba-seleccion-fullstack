import React from 'react';
import { Link } from 'react-router-dom';
import { Grid } from '@material-ui/core';
import CardDetail from './CardListDetail.jsx';
import { makeStyles } from '@material-ui/core/styles'

import styles from './style/CardList';

const useStyles = makeStyles(styles);

const CardList = props => {
  const { characters, page, rowsByPage } = props;
  const classes = useStyles();
  
  return (
    <Grid container spacing={4}>
      {characters.slice((page -1) * rowsByPage, (page) * rowsByPage).map((character, index) =>
        <Grid item key={index} xs={12} md={3} className={classes.gridHover}> 
          <Link to={`/characters/${character.id}`} className={classes.link}>
            <CardDetail character={character}/>
          </Link>
      </Grid>
      )}
    </Grid>
	);
};

export default CardList;

