const style = {
  avatarRoot: {
    height: 150,
    overflow: 'hidden'
  },
  avatarRounded: {
    borderBottomRightRadius: 0,
    borderTopRightRadius: 0
  },
  details: {
    display: 'flex',
    flexDirection: 'row',
  },
  cardContent: {
    flex: '1 0 auto',
  },
  gridContainer: {
    marginLeft: '0.5rem'
  },
  typographyAlign: {
    display: 'flex',
    textTransform: 'capitalize'
  },
  icon: {
    color: 'gray',
    width: 22,
    height: 22,
    marginRight: 4
  },
}

export default style