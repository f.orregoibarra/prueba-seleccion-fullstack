const style = {
  link : {
    textDecoration: 'none'
  },
  gridHover: {
    borderRadius: 4,
    '&:hover': {
      backgroundColor: 'gray',
    }
  }
}

export default style