const express = require('express');
const router = express.Router();

const { CharacterController } = require('../controllers/');

router.get('/', (req, res) => {
	try {
		CharacterController.findAll()
			.then(response => {
				res.status(200).json(response);
			})
			.catch(error => {
				res.status(404).json(error);
			});
	} catch (error) {
		res.status(404).json(error);
	}
});

router.get('/:id', (req, res) => {
	try {
		const id = req.params.id;
		CharacterController.findOne(id)
			.then(response => {
				res.status(200).json(response);
			})
			.catch(error => {
				res.status(404).json(error);
			});
	} catch (error) {
		res.status(404).json(error);
	}
});

module.exports = router;
