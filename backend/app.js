require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')
const app = express();
const port = process.env.PORT;
const { CharacterRoute } = require('./routes/');

app.use(cors())
app.use(bodyParser.json());

app.use('/characters', CharacterRoute);

app.listen(port, () => {
	console.log(`App listening at http://localhost:${port}`);
});