const axios = require('axios');

const client = axios.create({
	baseURL: process.env.API_GOT,
	timeout: 15000,
	headers: {
		'Content-Type': 'application/json',
	},
});

exports.response = options => {
	return new Promise((resolve, reject) => {
		try {
			client(options)
				.then(data => resolve(data.data))
				.catch(error => reject(error));
		} catch (error) {
			reject(error);
		}
	});
};
