/* 
  API Provider to handle all request for GOT API
*/

const { response } = require('./client');

module.exports.getBookCharacters = () => {
	const options = {
		url: `/book/characters`,
	};
	return response(options);
};
