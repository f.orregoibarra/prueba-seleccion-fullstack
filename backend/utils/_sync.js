const { CharacterController } = require('../controllers');
const { getBookCharacters } = require('../provider/provider');

sync = () => {
	getBookCharacters()
		.then(data => {
			CharacterController.createMany(data)
				.then(response => {
					console.log(response);
				})
				.catch(error => {
					console.log(error);
				})
				.then(exit => {
					return process.exit();
				});
		})
		.catch(error => {
			console.log(error);
			return process.exit();
		});
};

sync();
