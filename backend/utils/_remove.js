const { CharacterController } = require('../controllers');

remove = () => {
	CharacterController.removeAll()
		.then(response => {
			console.log(response);
		})
		.catch(error => {
			console.log(error);
		})
		.then(exit => {
			return process.exit();
		});
};

remove();
