const { CharacterModel } = require('../models');

/*
  SCRIPT METHODS
*/

exports.createMany = data => {
	return new Promise((resolve, reject) => {
		try {
			CharacterModel.insertMany(data, { ordered: false }) //ordered false for persistence
				.then(response => {
					resolve(`Number of records inserted: ${response.length}`);
				})
				.catch(error => {
					reject(error.message);
				});
		} catch (error) {
			reject(error.message);
		}
	});
};

exports.removeAll = () => {
	return new Promise((resolve, reject) => {
		try {
			CharacterModel.deleteMany({})
				.then(response => {
					resolve(`Number of records deleted: ${response.deletedCount}`);
				})
				.catch(error => {
					reject(error.message);
				});
		} catch (error) {
			reject(error.message);
		}
	});
};

/*
  API METHODS
*/

exports.findAll = () => {
	return new Promise(async (resolve, reject) => {
		try {
			const characters = await CharacterModel.find();
			resolve(characters);
		} catch (error) {
			reject({ message: error });
		}
	});
};

exports.findOne = id => {
	return new Promise(async (resolve, reject) => {
		try {
			const character = await CharacterModel.findById(id);
			resolve(character);
		} catch (error) {
			reject({ message: error });
		}
	});
};
