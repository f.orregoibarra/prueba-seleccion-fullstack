require('dotenv').config();
const mongoose = require('mongoose');
const CharacterModel = require('./character');

const DB_NAME = process.env.DB_NAME;
const DB_PASS = process.env.DB_PASS;
const DB_USER = process.env.DB_USER;

mongoose.connect(
  `mongodb+srv://${DB_USER}:${DB_PASS}@cluster0.b8baz.mongodb.net/${DB_NAME}`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useCreateIndex: true,
	},
	error => (error ? console.log(error) : console.log('Connected to Mongo DB'))
);

module.exports.CharacterModel = CharacterModel;
