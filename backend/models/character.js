const mongoose = require('mongoose');

const CharacterSchema = mongoose.Schema({
	id: { type: String, unique: true },
	name: { type: String },
	image: { type: String, default: '' },
	gender: { type: String, default: null },
	slug: { type: String },
	pagerank: {
		title: { type: String},
		rank: { type: Number },
	},
	house: { type: String, default: '' },
	books: { type: [String] },
	titles: { type: [String] }
});

module.exports = mongoose.model('Character', CharacterSchema);
